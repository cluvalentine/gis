package com.rizuki.gis;

import android.location.Geocoder;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        // Add a marker in Sydney and move the camera
        LatLng bandung = new LatLng(-6.914744, 107.609810);
        mMap.addMarker(new MarkerOptions().position(bandung).title("Marker in Bandung"));
        mMap.moveCamera( CameraUpdateFactory.newLatLngZoom(bandung , 14.0f) );

        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);

        mMap.getUiSettings().setZoomGesturesEnabled(true);
        mMap.getUiSettings().setRotateGesturesEnabled(true);

        MarkerOptions museumGeologi = new MarkerOptions()
                .position(new LatLng(-6.900524, 107.621445))
                .title("Museum Geologi")
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.museum_512));


        mMap.addMarker(new MarkerOptions()
                .position(new LatLng(-6.920810, 107.609589))
                .title("Museum Konferensi Asia Afrika")
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.museum_512)));

        mMap.addMarker(new MarkerOptions()
                .position(new LatLng(-6.902405, 107.619137))
                .title("Museum Gedung Sate")
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.museum_512)));

        mMap.addMarker(new MarkerOptions()
                .position(new LatLng(-6.937476, 107.603524))
                .title("Museum Sri Baduga")
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.museum_512)));

        mMap.addMarker(new MarkerOptions()
                .position(new LatLng( -6.878143, 107.587535))
                .title("Museum Barli")
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.museum_512)));

        mMap.addMarker(new MarkerOptions()
                .position(new LatLng(-6.852144, 107.493882))
                .title("Museum IPTEK")
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.museum_512)));

        mMap.addMarker(new MarkerOptions()
                .position(new LatLng(-6.902180, 107.619979))
                .title("Museum POS Indonesia")
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.museum_512)));

        mMap.addMarker(new MarkerOptions()
                .position(new LatLng(-6.917483, 107.611275))
                .title("Museum Mandala Wangsit Siliwangi")
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.museum_512)));

        mMap.addMarker(new MarkerOptions()
                .position(new LatLng(-6.859743, 107.594173))
                .title("Museum Pendidikan Nasional")
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.museum_512)));

        mMap.addMarker(new MarkerOptions()
                .position(new LatLng(-7.264556, 110.404612))
                .title("Museum Kereta Api Ambarawa")
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.museum_512)));

        mMap.addMarker(new MarkerOptions()
                .position(new LatLng(-6.921396, 107.609435))
                .title("Museum Wolff Schoemaker")
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.museum_512)));
    }
}
